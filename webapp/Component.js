sap.ui.define(
	["sap/ui/core/UIComponent"],
	function(UIComponent) {
		"use strict";

		/**
		 * @name be.fiddle.HideEmptygroups
		 * @instance
		 * @public
		 * @class
		 * <p>the HideEmptygroups plugin is instantiated and managed by the Fiori launchpad
		 * based on a target mapping in the configuration. You never, ever access it directly. Just
		 * let the fiori launchpad manage things.</p>
		 * <p>This plugin will override the standard visibility binding of each group and replace it with 
		 * a new formatter method. Based on the edit-mode and the number of tiles in the group, 
		 * we will return a new visibility boolean.</p>
		 * <p>The result is that the group will be invisible if it contains no tiles, unless the user
		 * is in personalization mode.</p>
		 */
		let HideEmptygroups = UIComponent.extend(
			"be.fiddle.hideEmptyGroups.Component",
			/**@lends be.fiddle.hideEmptyGroups.Component.prototype **/ {
				metadata: {
					manifest: "json"
				}
			}
		);

		/**
		 * @method init
		 * @public
		 * @instance
		 * @memberof be.fiddle.hideEmptyGroups.Component
		 * <p>during the initialization phase of the hideemptyGroups plugin we'll:
		 * <ul>
		 * 	<li> wait for the launchpad to be rendered</li>
		 * 	<li>then retrieve all instances of groups</li>
		 * 	<li>for these, override the default visibility binding and add a formatter</li>
		 * </ul>
		 * since it's done on the control, it should in theory remain even after a re-rendering
		 * or when tiles are added after an edit mode.
		 * That way, we don't need to touch the DOM, and we don't need to listen to all possible 
		 * re-render events </p>
		 * 
		 */
		HideEmptygroups.prototype.init = function(){
			if (UIComponent.prototype.init ) {
				UIComponent.prototype.init.apply(this,arguments);
			}

			//it might look weird that I use a timeout here to override the group bindings, but I noticed that from time to time
			//the plugin is loaded before the bindings are created. In the majority of cases, it loads afterwards, so we're okay
			//but sometimes, with apparently no reason,...
			this.repeater = setInterval( this.processGroups.bind(this) , 1000);
		};	

		/**
		 * @method destroy
		 * @public
		 * @instance
		 * @memberof be.fiddle.hideEmptyGroups.Component
		 * <p>object destructor: remove any pending event handlers and taskrepeaters</p>
		 */
		HideEmptygroups.prototype.destroy = function(){
			if (this.repeater) {
				clearInterval(this.repeater);
			}

			//ideally, we also re-enable the hidden groups:
			sap.ushell.Container.getServiceAsync("LaunchPage").then( function(launchpad){
				launchpad.getGroups().then(function(groups){
					//get all dom references to groups
					let domGroups = document.querySelectorAll(".sapUshellTileContainer") || [];
					domGroups.forEach(this.resetGroupVisibility.bind(this) );	

				}.bind(this) );
			}.bind(this));

			//and finally properly destroy the component
			UIComponent.prototype.destroy.apply(this,arguments);
		};

		/**
		 * @method processGroups
		 * @public
		 * @instance
		 * @memberof be.fiddle.hideEmptyGroups.Component
		 * <p>get groups and override the visibility of each</p>
		 */
		HideEmptygroups.prototype.processGroups = function(){
			//first attempt to get the groups from the launchpad, since the promise only resolves
			//when the groups are available in the shadow dom.
			sap.ushell.Container.getServiceAsync("LaunchPage").then( function(launchpad){
				launchpad.getGroups().then(function(groups){
					//get all dom references to groups
					let domGroups = document.querySelectorAll(".sapUshellTileContainer") || [];
					domGroups.forEach(this.overrideGroupVisibility.bind(this) );	
				}.bind(this) );
			}.bind(this));
		};

		/**
		 * @method overrideGroupVisibility
		 * @param {object} domGroup - A reference to the dom-object of the group
		 * @private
		 * @instance
		 * @memberof be.fiddle.hideEmptyGroups.Component
		 * <p>per group, override the default visibility binding and add a formatter</p> 
		 */
		HideEmptygroups.prototype.overrideGroupVisibility = function(domGroup){
			//from the domref, attempt to get the UI5 control
			let group = sap.ui.getCore().byId(domGroup.id);
			
			//how about we override the visibility binding of the group, and link it with the 
			//edit-mode? --> disadvantage: no CSS, so also no differentiation for mobile/tablet/desktop
			group.bindProperty("visible", {
				"parts":[
					"/tileActionModeActive", //Tsjakkaaa. This is the property of the edit button in the launchpad!
					"tiles" //and I also need to know if there are any tiles in the group
				],
				"formatter":this.isVisible
			}); //if you're empty, your visibility depends on the edit-mode	
		};

		/**
		 * @method resetGroupVisibility
		 * @param {object} domGroup - A reference to the dom-object of the group
		 * @private
		 * @instance
		 * @memberof be.fiddle.hideEmptyGroups.Component
		 * <p>per group, reset the default visibility binding and add a formatter</p> 
		 */
		HideEmptygroups.prototype.resetGroupVisibility = function(domGroup){
			//from the domref, attempt to get the UI5 control
			let group = sap.ui.getCore().byId(domGroup.id);			

			//and reset the group formatter to what it used to be
			group.bindProperty("visible", { 
				"parts":["/tileActionModeActive", "isGroupVisible",  "visibilityModes"], 
					formatter: function (tileActionModeActive, isGroupVisible, visibilityModes) {
						// Empty groups should not be displayed when personalization is off or
						// if they are locked or default group not in action mode
						if (!visibilityModes[tileActionModeActive ? 1 : 0]) {
								return false;
						}
						return isGroupVisible || tileActionModeActive;
				}
			}); 
		};

		/**
		 * @method isVisible
		 * @param {boolean} tileActionMode - this is the property on the launchpad that defines if the user is editing the launchpad config (personalization)
		 * @param {array} tiles - an array containing the tile-controls in the current group
		 * @returns {boolean} will the group be visible or not
		 * @memberof be.fiddle.hideEmptyGroups.Component
		 * 
		 * <p>The isVisible-method is a formatter. We do not call it directly ourselvs.
		 * The UI5 binding framework takes care of the call, everytime one of the bound properties
		 * changes. The result of the formatter function is that the group will be invisible if it 
		 * contains no tiles, unless the user is in personalization mode. </p>
		 */
		HideEmptygroups.prototype.isVisible = function( tileActionMode, tiles){
			//if we're in edit mode, we need to show all groups. even the empty ones.
			//if the group has at least one tile, we need to show it always (regardless of the edit mode)
			return tileActionMode || (tiles && tiles.length > 0);
		};

		return HideEmptygroups;
	}
);
