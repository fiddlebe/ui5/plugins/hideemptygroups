# Hide empty groups
This plugin is intended to be used in the SAP Fiori Launchpad.
It can happen that a launchpad contains many groups and many tiles, but not all tiles are always visible. Some tiles are only visible on certain device-types.
As a result, it could happen that you have completely empty groups. But eventhough the groups are emty, they still take up quite some space on your screen.
That is completely non-sensical, and is where this plugin comes into play.

The HideEmptyGroups plugin checks regularly if there are any groups without any tiles. In case it finds such plugins, it changes the visibility-binding of the group to use a new formatter function. The new function takes in the edit-modus and the number of tiles in the group. Based on those 2 parameters, we decide whether to show the empty group, or not.

When the plugin is destroyed, we reset the visibility binding again to the original situation.

![hideEmptyGroups](https://gitlab.com/fiddlebe/ui5/plugins/hideemptygroups/uploads/0ed75940aa787106bb9506f977ee1bc7/hideEmptyGroups.gif)
